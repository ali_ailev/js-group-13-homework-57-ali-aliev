import { Group } from './group.model';
import { EventEmitter } from '@angular/core';

export class GroupService {
  groupChange = new EventEmitter<Group[]>();

  private group: Group[] = [
    new Group('Molodci', ['Arli Artur', 'Cai Artur']),
    new Group('Duraki', ['Aliev Ali'])
  ]

  getGroup() {
    return this.group.slice();
  }

  addGroup(group: Group) {
    this.group.push(group);
    this.groupChange.emit(this.group)
  }
}
