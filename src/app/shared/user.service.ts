import { User } from './user.model';
import { EventEmitter } from '@angular/core';

export class UserService {
  userChange = new EventEmitter<User[]>();

  private users: User[] = [
    new User('Aliev Ali', 'ali-aliev-2002@inbox.ru', true, 'admin'),
    new User('Cai Artur', 'somemail@mail.ru', false, 'user'),
    new User('Arli Artur', 'somegmail@gmail.com', false, 'redactor')
  ]

  getUsers() {
    return this.users.slice();
  }

  addUser(user: User) {
    this.users.push(user);
    this.userChange.emit(this.users)
  }

}
