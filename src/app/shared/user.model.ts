export class User {
  constructor(
    public name: string,
    public email: string,
    public activeStatus: boolean,
    public role: string,
  ) {}
}
