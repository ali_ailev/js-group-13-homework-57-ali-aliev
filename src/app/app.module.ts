import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { UsersCreatorComponent } from './users-creator/users-creator.component';
import { UserBlockComponent } from './users/user-block/user-block.component';
import { UserService } from './shared/user.service';
import { FormsModule } from '@angular/forms';
import { GroupCreatorComponent } from './group-creator/group-creator.component';
import { GroupService } from './shared/group.service';
import { GroupsComponent } from './groups/groups.component';
import { GroupBlockComponent } from './groups/group-block/group-block.component';

@NgModule({
    declarations: [
        AppComponent,
        UsersComponent,
        UsersCreatorComponent,
        UserBlockComponent,
        GroupCreatorComponent,
        GroupCreatorComponent,
        GroupsComponent,
        GroupBlockComponent
    ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [UserService,GroupService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
