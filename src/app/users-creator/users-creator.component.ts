import { Component, ViewChild, ElementRef } from '@angular/core';
import { User } from '../shared/user.model';
import { UserService } from '../shared/user.service';

@Component({
  selector: 'app-users-creator',
  templateUrl: './users-creator.component.html',
  styleUrls: ['./users-creator.component.css']
})
export class UsersCreatorComponent {
  @ViewChild('nameInput') nameInput!: ElementRef;
  @ViewChild('emailInput') emailInput!: ElementRef;
  @ViewChild('role') role!: ElementRef;
  @ViewChild('trueStatus') trueStatus!: ElementRef;
  @ViewChild('falseStatus') falseStatus!: ElementRef;

  constructor(private userService:UserService) {
  }

  createUser() {
    const name = this.nameInput.nativeElement.value;
    const email = this.emailInput.nativeElement.value;
    let activeStatus = false;
    const role = this.role.nativeElement.value;
    switch (this.trueStatus.nativeElement.checked) {
      case true:
        activeStatus = true;
        break;
      case false:
        activeStatus = false;
        break;
    }
    const user = new User(name, email, activeStatus, role);
    this.userService.addUser(user);
  }
}
