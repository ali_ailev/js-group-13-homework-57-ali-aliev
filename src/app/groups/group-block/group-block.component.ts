import { Component, Input } from '@angular/core';
import { Group } from '../../shared/group.model';

@Component({
  selector: 'app-group-block',
  templateUrl: './group-block.component.html',
  styleUrls: ['./group-block.component.css']
})
export class GroupBlockComponent {
  @Input() group!: Group;
}
