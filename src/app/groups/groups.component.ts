import { Component } from '@angular/core';
import { GroupService } from '../shared/group.service';
import { User } from '../shared/user.model';
import { Group } from '../shared/group.model';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.css']
})
export class GroupsComponent {
  constructor(private groupService: GroupService) {}
  groups: Group[] = this.groupService.getGroup();
}
