import { Component, ElementRef, ViewChild } from '@angular/core';
import { Group } from '../shared/group.model';
import { GroupService } from '../shared/group.service';

@Component({
  selector: 'app-group-creator',
  templateUrl: './group-creator.component.html',
  styleUrls: ['./group-creator.component.css']
})
export class GroupCreatorComponent {
  @ViewChild('titleInput') titleInput!: ElementRef;

  constructor(private groupService: GroupService) {
  }

  createGroup() {
    const title = this.titleInput.nativeElement.value;
    const group = new Group(title,[]);
    this.groupService.addGroup(group);
  }
}
